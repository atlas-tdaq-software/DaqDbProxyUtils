tdaq_package()

tdaq_add_library(DaqDbProxyUtils src/*.cxx)
tdaq_add_executable(proxyTest src/proxyTest.cc LINK_LIBRARIES DaqDbProxyUtils ROOT)

# FIXME: generate dictionary with ROOTCINT at build time
