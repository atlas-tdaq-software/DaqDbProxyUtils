//
// DBproxy log connection summary decription
//
// Update: Su Dong  2007-09-05  Initial release 
//
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

#include "DaqDbProxyUtils/transaction.h"
#include "DaqDbProxyUtils/connectionSummary.h"

#if RootApp
ClassImp(connectionSummary); 
#endif

// default ctor 
connectionSummary::connectionSummary() { 
  _node = 0;
  _connection = 0;
  _time_start = 0; 
  _time_end = 0;
  _nmessage = 0;
  for(int i=0;i<transaction::nComType;i++) { 
    _com_mult[i] = 0;
    _com_sumbytes[i] = 0;
    _com_sumtime[i]  = 0;
  }
  _transInit =0;
}  

// Update the summary with a new transaction 
int connectionSummary::Update(transaction* trans) { 
  if(trans->Node()!=_node || trans->Connection()!=_connection) {
    printf("*** connectionSummary update ID mismatch\n");
    return 0;
  }
  _time_end = trans->TimeSec() + 1e-6*trans->TimeUsec(); 
  _nmessage++;
  int commtype = trans->ComType();
  _com_mult[commtype]++;
  _com_sumbytes[commtype] += trans->Nbytes();
  _com_sumtime[commtype] += 1e-6*trans->Delta(); 
  return 1;
}  

// Initialize the summary at first transaction 
int connectionSummary::Initialize(transaction* trans){ 
  _node = trans->Node();
  _connection = trans->Connection();
  _time_start = trans->TimeSec() + 1e-6*trans->TimeUsec();
  _nmessage = 0; 
  for(int i=0;i<transaction::nComType;i++) { 
    _com_mult[i] = 0;
    _com_sumbytes[i] = 0;
    _com_sumtime[i]  = 0;
  }
  _transInit = trans;
  return Update(trans);
}  

// Various types of byte counts

int connectionSummary::AllBytes() const {
  int sum = 0;
  for(int i=0;i<transaction::nComType;i++) { 
    sum += _com_sumbytes[i];
  }  
  return sum;
}
int connectionSummary::NonCacheBytes() const {
  int sum = 0;
  for(int i=0;i<transaction::nComType;i++) { 
    if(TransInit()->ComIsNonCacheReply(i)) sum += _com_sumbytes[i];
  }  
  return sum;
}
int connectionSummary::CacheBytes() const {
  int sum = 0;
  for(int i=0;i<transaction::nComType;i++) { 
    if(TransInit()->ComIsCacheReply(i)) sum += _com_sumbytes[i];
  }  
  return sum;
}

// Various types of DeltaTime sums

double connectionSummary::AllTime() const {
  double sum = 0;
  for(int i=0;i<transaction::nComType;i++) { 
    sum += _com_sumtime[i];
  }  
  return sum;
}
double connectionSummary::NonCacheTime() const {
  double sum = 0;
  for(int i=0;i<transaction::nComType;i++) { 
    if(TransInit()->ComIsNonCacheReply(i)) sum += _com_sumtime[i];
  }  
  return sum;
}
double connectionSummary::CacheTime() const {
  double sum = 0;
  for(int i=0;i<transaction::nComType;i++) { 
    if(TransInit()->ComIsCacheReply(i)) sum += _com_sumtime[i];
  }  
  return sum;
}

// print table dump

void connectionSummary::dump() { 
  printf("Node:Connx=%3x:%5d  StartTime=%18.6f TimeSpan=%10.6f\n",
	 _node,_connection,_time_start,_time_end-_time_start);
  printf("Number of messages = %8i\n",_nmessage); 
  printf("Command                  Mult      ByteSum         TimeSum\n");
  for(int i=0; i<transaction::nComType; i++) { 
    string ComName;
    TransInit()->ComName(i,ComName); 
    printf("%18s %9i    %9i    %12.6f\n",
	ComName.c_str(), _com_mult[i],_com_sumbytes[i],_com_sumtime[i]);    
  } 
  return;
}

// print two line summary 

void connectionSummary::summary(const double T0) {
  double T1   = _time_start - T0;
  double delT = _time_end - _time_start; 
  printf("Node:Connx=%3x:%5d Tstart/span=%8.4f/%8.4f dTime=%8.4f/%8.4f/%8.4f\n",
         _node,_connection,T1,delT,
         AllTime(),NonCacheTime(),CacheTime());
  printf("           No. of msg=%6i    All/NonCache/Cache Bytes=%8i/%8i/%8i\n",
         _nmessage,AllBytes(),NonCacheBytes(),CacheBytes());

  return;
}
