//
// DBproxy Log file analysis test
// 
//  Usage:  ./proxyTest [filename] [domain] [version] [printOpt1] [printOpt2]  
//  filename:   is the DBproxy logfile file name 
//  Domain:     p = preseries 
//              x = XPUs  
//              s = SLAC
//  Version:    0 = Take delta time as in the log file
//              1 = Recalc delta for version-3 with 64 bit time stamp bug.   
//  printOpt1:  raw transaction list dump level (default=1) 
//              0=no print   
//              1=one line per 10000 transactions
//              2=one line per 1000 transactions
//  printOpt2:  connection summary dump
//              0=no print 
//              1=2 lines per connection summary 
//              2=full connection summary table dump
//
//  Update: Su Dong  2007-09-05  Initial release 
//          Su Dong  2008-07-14  16 bit IPmask; Allow logfile errors.  
//  
#include <list>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstring>

#define RootApp 0

using namespace std;

#include "DaqDbProxyUtils/transaction.h"
#include "DaqDbProxyUtils/connectionSummary.h"
#include "DaqDbProxyUtils/proxyLogList.h"
#include "DaqDbProxyUtils/proxyConnxList.h"
class transaction;
class connectionSummary;
class proxyLogList;
class proxyConnxList;

int main(int argc, const char* argv[])
{
  //
  // Check argument file name
  //
  char fileName[120];
  int version   = 0;
  int printOpt1 = 0;
  int printOpt2 = 0;

  if( argc<2 || argc>6 ) { 
    cout << "Execution argument count error. Usage should be: " << endl;
    cout << " ./proxyTest [filename] [domain] [version] [printOpt1] [printOpt2]" << endl;
    return 1;
  }

  memcpy(fileName,argv[1],strlen(argv[1])+1);
  cout << fileName << endl;
  cout << "  " << endl;

  unsigned maskIP = 0xa920000; // default XPU;
  if( argc>=3 ) {
    char network[16];
    sscanf(argv[2],"%s",network); 
    if(network[0]=='P'||network[0]=='p')      {maskIP= 0xa950000;} // preseries
    else if(network[0]=='X'||network[0]=='x') {maskIP= 0xa920000;} // XPU
    else if(network[0]=='S'||network[0]=='s') {maskIP=0x864f0000;} // SLAC 
    else { sscanf(network,"%x",&maskIP); }
    cout << "Domain IP address mask = " << hex << maskIP << dec << endl;
  }
    
  if( argc>=4 ) sscanf(argv[3],"%d",&version); 
  if( argc>=5 ) sscanf(argv[4],"%d",&printOpt1); 
  if( argc==6 ) sscanf(argv[5],"%d",&printOpt2);  

  //
  // Read in DBproxy log file to create the full transaction list 
  //
  proxyLogList* Ltrans = new proxyLogList( 
              fileName, version, maskIP, printOpt1);
  cout << "Reading DBproxy log file..." << endl;
  int nerrors = Ltrans->execute();  
  if(nerrors>99) {
    cout << "** Abort due to too many log file read error." << endl;  
    return 0;
  } else if(nerrors>0) { 
    cout << endl << "** LOG FILE ERROR DETECTED = " << nerrors << 
      ". RESULTS BELOW IS UNRELIABLE. " << endl;  
  }
  //
  // Record the start time
  //
  list<transaction>* transList = Ltrans->TheList(); 
  transaction* transFirst = &(*transList->begin());
  double T0 = transFirst->Time();

  //
  // Sort the transaction list by node and form connection summary list
  //
  proxyConnxList* Lconnx = new proxyConnxList( printOpt2, transList );  
  nerrors = Lconnx->execute();
  if(nerrors>0) {
    cout << "** Abort due to connection summary list error." << endl;  
    return 0;
  }
  list<connectionSummary> *connxList = Lconnx->ConnxList();  

  //
  // Analyze and tabulate summary
  //
  const int MAXNODE  = 64;
  const int MAXCONNX = 4000;
  int nodeList[MAXNODE];
  int nconnect[MAXNODE];
  int nbytesum[MAXNODE][3];
  double dtimesum[MAXNODE][3];
  double timemark[MAXNODE][2];
  int nmessage[MAXCONNX][MAXNODE];
  for(int i=0;i<MAXNODE;i++) nconnect[i] = 0;
  for(int i=0;i<MAXNODE;i++) {
    for(int j=0;j<MAXCONNX;j++) {
      nmessage[j][i] = 0;
    }
    for(int j=0;j<3;j++) { 
      nbytesum[i][j] = 0;
      dtimesum[i][j] = 0;
    }
  } 

  int NodeFirst = -1;
  int LastNode  = -1;
  int nActiveNodes = 0;
  int indNode = 0;
  int indConnx= 0;
  //
  //  Look through the connection list to fill local summary buffer
  //
  list<connectionSummary>::iterator iter;
  for(iter=connxList->begin(); iter!=connxList->end(); ++iter) {
    connectionSummary* connxSum = &(*iter);
    if(connxSum->Node()!=LastNode) {
      if(NodeFirst<0) NodeFirst = connxSum->Node();
      indConnx = 0;
      nodeList[nActiveNodes++] = connxSum->Node();
      indNode = nActiveNodes-1;
      timemark[indNode][0] = connxSum->TimeStart() - T0; 
    } else {
      indConnx++;
      if(indConnx>MAXCONNX)
        cout << "**ERROR: exceeded max connection/node " << endl;
    }
    nconnect[indNode]++;
    nmessage[indConnx][indNode] = connxSum->Nmessage();
    LastNode = connxSum->Node();
    timemark[indNode][1] = connxSum->TimeEnd() - T0; 
    nbytesum[indNode][0] += connxSum->AllBytes();  
    nbytesum[indNode][1] += connxSum->CacheBytes();      
    nbytesum[indNode][2] += connxSum->NonCacheBytes();   
    dtimesum[indNode][0] += connxSum->AllTime();  
    dtimesum[indNode][1] += connxSum->CacheTime();      
    dtimesum[indNode][2] += connxSum->NonCacheTime();      
  }

 // Tabulate one column per node summary

  int nskip = nodeList[nActiveNodes-1] - nodeList[0] + 1 - nActiveNodes;
  cout << " " << endl;
  cout << "**Summary of by node message counts for each connection" << endl;
  printf("Total active nodes =%3d  First/last nodes: %4x %4x   Skipped nodes =%2d\n\n",
         nActiveNodes,nodeList[0],nodeList[nActiveNodes-1],nskip);

  int nblock = nActiveNodes/8 + 1;

  for (int i=0;i<nblock;i++) {
    int ind1 = 8*i;
    int ind2 = 8*i+7;
    if(ind2>=nActiveNodes) ind2 = nActiveNodes-1;
    int nloop = ind2 - ind1+1;
    // Node names
    printf("Node    ");
    for (int j=0;j<nloop;j++ ) {
      int node = nodeList[ind1+j];;
      printf("%9x",node);
    }
    // no. of connections per node
    printf("\n");
    printf("Nconnect");
    int maxc = 0;
    for (int j=0;j<nloop;j++ ) {
      printf("%9d",nconnect[ind1+j]);
      if(nconnect[ind1+j]>maxc) maxc = nconnect[ind1+j];
    }
    // Start/End times 
    printf("\n");
    printf("Time Ini");
    for (int j=0;j<nloop;j++ ) {
      printf("%9.3f",timemark[ind1+j][0]);
    }
    printf("\n");
    printf("Time End");
    for (int j=0;j<nloop;j++ ) {
      printf("%9.3f",timemark[ind1+j][1]);
    }
    // Bytes for different type of data queries 
    printf("\n");
    printf("BytesAll");
    for (int j=0;j<nloop;j++ ) {
      printf("%9i",nbytesum[ind1+j][0]);
    }
    printf("\n");
    printf(" Cached ");
    for (int j=0;j<nloop;j++ ) {
      printf("%9i",nbytesum[ind1+j][1]);
    }
    printf("\n");
    printf(" NoCache");
    for (int j=0;j<nloop;j++ ) {
      printf("%9i",nbytesum[ind1+j][2]);
    }
    // Delta time for different type of data queries 
    printf("\n");
    printf("dTimeAll");
    for (int j=0;j<nloop;j++ ) {
      printf("%9.3f",dtimesum[ind1+j][0]);
    }
    printf("\n");
    printf(" Cached ");
    for (int j=0;j<nloop;j++ ) {
      printf("%9.3f",dtimesum[ind1+j][1]);
    }
    printf("\n");
    printf(" NoCache");
    for (int j=0;j<nloop;j++ ) {
      printf("%9.3f",dtimesum[ind1+j][2]);
    }

    // List of connection message conunts
    printf("\n");
    printf("Connection message counts for each connection:\n");
    for(int m=0;m<maxc;m++) {
      printf("%4d    ",m);
      for (int j=0;j<nloop;j++ ) {
        printf("%9d",nmessage[m][ind1+j]);
      }
      printf("\n");
    }
    printf("\n");
  }  

  return 0;
}
