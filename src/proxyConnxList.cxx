//
// Routine to make an STL list of DBproxy log connection summaries
// 
// Update: Su Dong  2007-09-05  Initial release 
//
#include <list>
#include <iostream>
#include <fstream>

using namespace std;

#include "DaqDbProxyUtils/transaction.h"
#include "DaqDbProxyUtils/connectionSummary.h"
#include "DaqDbProxyUtils/proxyConnxList.h"

class transaction;
class connectionSummary;

#if RootApp
ClassImp(proxyConnxList);
#endif

// ctor 

proxyConnxList::proxyConnxList(
  int printLevel, 
  list<transaction> *transList)
{
  _printLevel = printLevel;
  _transList  = transList; 
}

//
// Executing the transaction list sorting and 
// connection summary list creation 
// Note:  _transList is the list of individual DBproxy transactions
//        (assume to be unsorted in original time sequence)
//        and it will be sorted (node:connx:time) as output
//        _connxList is the created output connection summary list.
//
int proxyConnxList::execute(){

  // Take note of the very first entry in the log for T0 
  transaction* transFirst = &(*_transList->begin());
  double T0 = transFirst->Time();  

  // Sort the list to group entries from same node:connection together
  cout << "  " << endl;
  cout << "Sorting log entry list to group by node:connection" << endl; 
  cout << "  " << endl;
  _transList->sort();    
     
  int LastNode  = -1;
  int LastConnx = -1;
  connectionSummary* summary = new connectionSummary();
  bool LastSummary = 0; 

  list<transaction>::iterator iter;
  for(iter=_transList->begin(); iter!=_transList->end(); ++iter) {
    bool newNode  = 0;
    bool newConnx = 0;
    transaction* aTrans  = &(*iter); 
    if(aTrans->Node()!=LastNode) { 
      newNode = 1;
      newConnx = 1;
    } else { 
      if(aTrans->Connection()!=LastConnx) { 
        newConnx = 1; 
      } 
    }
   
    // at the end of each connection summarize it
    if(newNode||newConnx) {
      if(LastSummary) { 
        _connxList.push_back(*summary);
        if(_printLevel==1) { summary->summary(T0); }
        else if(_printLevel>=2) { summary->dump(); } 
      }
      summary->Initialize(aTrans); 
      LastSummary = 1;      
    } else { 
      summary->Update(aTrans);
    }
    LastNode  = aTrans->Node();
    LastConnx = aTrans->Connection();
  }              
  _connxList.push_back(*summary);
  if(_printLevel==1) { summary->summary(T0); }
  else if(_printLevel>=2) { summary->dump(); } 

  return 0;
}
