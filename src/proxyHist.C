//
// Root Macro:  DBproxy Log file analysis to output histograms   
//
// Update: Su Dong 2007-09-04  Initial release
//         Su Dong 2007-11-04  Add max time control; nconnx_vs_node scale 
//         Su Dong 2008-07-14  Allow log errors with warning 
//         Su Dong 2008-07-20  Add dTime vs Bytes with controlled max scale 
//
#include <list>
#include <iostream>
#include <string>
#include <cmath>
using namespace std; 

#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>

#define RootApp 1

#include "DaqDbProxyUtils/transaction.h"
#include "DaqDbProxyUtils/connectionSummary.h"
#include "DaqDbProxyUtils/proxyLogList.h"
#include "DaqDbProxyUtils/proxyConnxList.h"
class transaction;
class connectionSummary;
class proxyLogList;
class proxyConnxList;

#include "DaqDbProxyUtils/proxyHist.h"

ClassImp(proxyHist)

proxyHist::proxyHist() { 
  _timemax = -1.;  //  -ve value denote dynamic max from data 
}

//
// Rounding histogram bin to sensible values
//
void proxyHist::HistBinRounding(double vmax, int& Nbin, double& rmax){ 
  double vupper  = 1.18*vmax;
  double vlog    = log10(vupper);
  int    pow10   = int(vlog);
  if(vlog<0) pow10--;
  double apow10 = pow10;
  double vunorm  = 100*vupper/pow(10,apow10);
  int    Inorm   = int(vunorm/10)*10;

  int maxval[4] = {  0, 160,  400, 800 };
  int binval[4] = {  2,   5,   10,  20 };
  int ind = 0;
  for(int i=1;i<4;i++) { 
    if(Inorm>maxval[i]) ind = i;
  }
  int binnorm = binval[ind];
  Nbin = int(int(vunorm/binnorm)/5)*5; 
  rmax = binnorm*Nbin*pow(10,apow10)/100;
  return;
}
//
// Default automatic histo output file name based on input file name
//
void proxyHist::OutputName(char* fileName, string& outName)
{
  int NotFound = string::npos;
  string LogFile = std::string(fileName);
  int ind_slash = LogFile.find_last_of("/");
  int ind_out = LogFile.find(".out");
  if(ind_out==NotFound) ind_out = LogFile.find(".log");
  int ind_start = ind_slash+1;
  if(ind_slash==NotFound) ind_start= 0;
  if(ind_out==NotFound) {
    outName = LogFile.substr(ind_start); 
  } else {
    int nchar = ind_out - ind_start;
    outName = LogFile.substr(ind_start,nchar);
  }
}   
//
// Main analysis driver
//
int proxyHist::execute(char* fileName, int version, int domain)
{

  cout << fileName << endl;
  cout << "  " << endl;

  int printOpt1 = 0; 
  int printOpt2 = 0;  

  //
  // Read in DBproxy log file to create the full transaction list 
  //
  cout << "Reading DBproxy log file..." << endl;
  proxyLogList* Ltrans = new proxyLogList(
    fileName, version, domain, printOpt1);  
  int nerrors = Ltrans->execute(); 
  if(nerrors>99) {
    cout << "** Abort due to too many log file read error." << endl;  
    return 9999;
  } else if(nerrors>0) { 
    cout << endl << "** LOG FILE ERROR DETECTED = " << nerrors << 
      ". RESULTS PLOTTED IS UNRELIABLE. " << endl;  
  }
  list<transaction>* transList = Ltrans->TheList(); 

  //
  // Record the start time
  //
  transaction* transFirst = &(*transList->begin());
  double T0 = transFirst->Time();

  //
  // Sort the transaction list by node and form connection summary list
  //
  proxyConnxList* Lconnx = new proxyConnxList( printOpt2, transList ); 
  nerrors = Lconnx->execute();  
  if(nerrors>0) {
    cout << "** Abort due to summary list error." << endl;  
    return nerrors;
  } else {
    cout << "Sorted connection summary list." << endl;
  }
  list<connectionSummary>* connxList = Lconnx->ConnxList();  

  //
  // Analyze 
  //
  //*** Connection summary histograms
  //
  const int MAXNODE  = 128;
  const int MAXNMAP  = 8000;
  int nodeList[MAXNODE];
  int nodeMap[MAXNMAP];
  int nconnect[MAXNODE];
  double nbytesum[MAXNODE][3];  
  double dtimesum[MAXNODE][3];
  double timemark[MAXNODE][2];
  int nmessage[MAXNODE];
  for(int i=0;i<MAXNMAP;i++) nodeMap[i] = -1;
  for(int i=0;i<MAXNODE;i++) nconnect[i] = 0;
  for(int i=0;i<MAXNODE;i++) nmessage[i] = 0;
  for(int i=0;i<MAXNODE;i++) {
    timemark[i][0] = 9.9e21;
    timemark[i][1] = 0.;
    for(int j=0;j<3;j++) { 
      nbytesum[i][j] = 0;
      dtimesum[i][j] = 0;
    }
  } 

  int NodeFirst = -1;
  int NodeLast  = -1;
  int nActiveNodes = 0;
  int indNode = 0;
  int indConnx= 0;
  //
  double max_connx = 0;
  double max_nmsg  = 0;
  double max_bytes = 0;
  double max_endtime = 0;
  double max_deltime = 0;
  double max_Qdtime  = 0;

  //
  //  Look through the connection list to fill local summary buffer
  //
  list<connectionSummary>::iterator iter;
  for(iter=connxList->begin(); iter!=connxList->end(); ++iter) {
    connectionSummary* connxSum = &(*iter);
    bool newNode  = 0;
    if(connxSum->Node()!=NodeLast) {
      newNode = 1;
      int NodeNum = connxSum->Node(); 
      if(NodeFirst<0) NodeFirst = NodeNum;
      indConnx = 0;
      if(nActiveNodes>=MAXNODE) { 
        cout << "** Abort due to too many nodes." << endl;  
        return 9999;        
      } 
      nodeList[nActiveNodes++] = NodeNum;      
      indNode = nActiveNodes-1;
      int nodeLoc = NodeNum - NodeFirst;
      if(nodeLoc>=MAXNMAP) {  
        cout << "Node number " << hex << NodeNum << " out of range. "
             << "First node is " << NodeFirst << dec << endl; 
      } else { 
        nodeMap[nodeLoc] = indNode;
      }
      double tstart = connxSum->TimeStart() - T0; 
      timemark[indNode][0] = min(tstart,timemark[indNode][0]); 
    } else {
      indConnx++;
    }
    if(connxSum->AllBytes()>9.e6||connxSum->AllBytes()<0) {
      cout << "Allbytes = " << connxSum->AllBytes() << endl; 
    }
    nconnect[indNode]++;
    nmessage[indNode] += connxSum->Nmessage();
    NodeLast = connxSum->Node();
    double tend = connxSum->TimeEnd() - T0; 
    timemark[indNode][1] = max(tend,timemark[indNode][1]); 
    nbytesum[indNode][0] += connxSum->AllBytes();  
    nbytesum[indNode][1] += connxSum->CacheBytes();      
    nbytesum[indNode][2] += connxSum->NonCacheBytes();   
    dtimesum[indNode][0] += connxSum->AllTime();  
    dtimesum[indNode][1] += connxSum->CacheTime();      
    dtimesum[indNode][2] += connxSum->NonCacheTime();
  }
  //
  // Convert nbytes into kbytes
  //DaqDbProxyUtilsCint.cxx
  for(int iNode=0;iNode<nActiveNodes;iNode++) { 
    for(int i=0;i<3;i++) { 
      nbytesum[iNode][i] = 1.0e-3*nbytesum[iNode][i];
    }
  }         

  cout << "Statistics buffer filling complete." << endl;

  //
  // Find maximum values for histogram bounds
  // 
  for(int i=0;i<nActiveNodes;i++) { 
    if(nconnect[i]>max_connx)      max_connx   = nconnect[i]; 
    if(nmessage[i]>max_nmsg)       max_nmsg    = nmessage[i]; 
    if(nbytesum[i][0]>max_bytes)   max_bytes   = nbytesum[i][0]; 
    if(dtimesum[i][0]>max_deltime) max_deltime = dtimesum[i][0]; 
    if(dtimesum[i][1]>max_Qdtime)  max_Qdtime  = dtimesum[i][1];  
    if(dtimesum[i][2]>max_Qdtime)  max_Qdtime  = dtimesum[i][2];  
    if(timemark[i][1]>max_endtime) max_endtime = timemark[i][1];
  }
  //
  // Manual control of time history end time
  //
  if(TimeMax()>0 && TimeMax()<max_endtime) {
    cout << "Time history time window truncated to manual control max = " 
         << TimeMax() << " (data max is " << max_endtime << ")" << endl;
    max_endtime = TimeMax();
  }
  
  //
  // Histogram the results 
  //
  int Nbins;
  double vmax;
  int Nhnode = nActiveNodes;
  cout << "Nhnode = " << Nhnode << endl;   
  HistBinRounding(max_connx,Nbins,vmax);
  cout << "Nbins,max_connx " << Nbins << "  " << max_connx << " vmax= " << vmax << endl; 
  TH2F* h_connx_vs_node = new TH2F("connx_vs_node","Connections",
		    Nhnode,0,float(Nhnode), Nbins,0,vmax);
  HistBinRounding(max_nmsg,Nbins,vmax);
  cout << "Nbins,max_nmsg " << Nbins << "  " << max_nmsg << endl;
  TH2F* h_nmsg_vs_node = new TH2F("nmsg_vs_node","messages vs Node",
		    Nhnode,0,float(Nhnode), Nbins,0,vmax);
  HistBinRounding(max_bytes,Nbins,vmax);
  cout << "Nbins,max_bytes " << Nbins << "  " << max_bytes << endl; 
  TH2F* h_Allbytes_vs_node = new TH2F("Allbytes_vs_node","All bytes",
		    Nhnode,0,float(Nhnode), Nbins,0,vmax);
  TH2F* h_Cbytes_vs_node = new TH2F("Cbytes_vs_node","Cached bytes",
		    Nhnode,0,float(Nhnode), Nbins,0,vmax);
  TH2F* h_Qbytes_vs_node = new TH2F("Qbytes_vs_node","Non-Cached bytes",
		    Nhnode,0,float(Nhnode), Nbins,0,vmax);
  HistBinRounding(max_deltime,Nbins,vmax);
  TH2F* h_Alldtime_vs_node = new TH2F("Alldtime_vs_node","All dtime",
		    Nhnode,0,float(Nhnode), Nbins,0,vmax);
  max_Qdtime = 1.0e3*max_Qdtime;  // msec  
  HistBinRounding(max_Qdtime,Nbins,vmax);
  TH2F* h_Cdtime_vs_node = new TH2F("Cdtime_vs_node","Cached dtime",
		    Nhnode,0,float(Nhnode), Nbins,0,vmax);
  TH2F* h_Qdtime_vs_node = new TH2F("Qdtime_vs_node","Non-Cached dtime",
		    Nhnode,0,float(Nhnode), Nbins,0,vmax);
  HistBinRounding(max_endtime,Nbins,vmax);
  TH2F* h_startTime_vs_node = new TH2F("StartTime_vs_node","Start time",
		    Nhnode,0,float(Nhnode), Nbins,0,vmax);
  TH2F* h_endTime_vs_node = new TH2F("EndTime_vs_node","End time",
		    Nhnode,0,float(Nhnode), Nbins,0,vmax);
  TH2F* h_node_vs_endtime = new TH2F("Node_vs_EndTime","Node vs End time",
		    Nbins,0,vmax, Nhnode,0,float(Nhnode) );
 
  for(int i=0;i<nActiveNodes;i++) { 
    float hnode = i ;
    h_connx_vs_node->Fill(hnode,float(nconnect[i]),1.);
    h_nmsg_vs_node->Fill(hnode,float(nmessage[i]),1.);
    h_Allbytes_vs_node->Fill(hnode,float(nbytesum[i][0]),1.);
    h_Cbytes_vs_node->Fill(hnode,float(nbytesum[i][1]),1.);
    h_Qbytes_vs_node->Fill(hnode,float(nbytesum[i][2]),1.);
    h_Alldtime_vs_node->Fill(hnode,dtimesum[i][0],1.);
    double Ctime_msec = 1.0e3*dtimesum[i][1];  // msec
    h_Cdtime_vs_node->Fill(hnode,Ctime_msec,1.);
    double Qtime_msec = 1.0e3*dtimesum[i][2];  // msec
    h_Qdtime_vs_node->Fill(hnode,Qtime_msec,1.);
    h_startTime_vs_node->Fill(hnode,timemark[i][0],1.);
    h_endTime_vs_node->Fill(hnode,timemark[i][1],1.);
    h_node_vs_endtime->Fill(timemark[i][1],hnode,1.);
  }

  cout << "Summary histograms done." << endl;

  //
  //*** Single transaction histograms
  //
  // First find out the max dtime,bytes per transaction 
  //
  double max_dt=0;
  double max_tbytes=0;
  double max_dtmaxb=0;
  double max_comdt[transaction::nComType];
  for(int i=0;i<transaction::nComType;i++) { max_comdt[i]=0; } 

  list<transaction>::iterator it;
  for(it=transList->begin(); it!=transList->end(); ++it) {
    transaction* aTrans = &(*it);
    double Deltime = aTrans->Delta();
    int ComType= aTrans->ComType();
    if(aTrans->ComIsCacheReply() || aTrans->ComIsNonCacheReply()) {
      if(Deltime>max_dt) max_dt=aTrans->Delta();
      if(aTrans->Nbytes()>max_tbytes){
        max_tbytes=aTrans->Nbytes();
        max_dtmaxb=Deltime;
      } else if(aTrans->Nbytes()==max_tbytes) { 
        if(Deltime>max_dtmaxb) max_dtmaxb = Deltime;
      }
    }
    if(Deltime>max_comdt[ComType])max_comdt[ComType] = Deltime;  
  } 
  max_dt     *= 1.0e-3; // msec
  max_tbytes *= 1.0e-3; // kbytes
  max_dtmaxb *= 1.0e-3; // msec 
  for(int i=0;i<transaction::nComType;i++) { max_comdt[i] *=1.e-3; } 

  //
  // Book histograms for individual transaction bytes/delta time 2D plots
  //
  int Nbin_dt, Nbin_tbytes, Nbin_dtmaxb;
  double vmax_dt, vmax_tbytes, vmax_dtmaxb;  
  HistBinRounding(max_dt,Nbin_dt,vmax_dt);
  HistBinRounding(max_tbytes,Nbin_tbytes,vmax_tbytes);
  HistBinRounding(max_dtmaxb,Nbin_dtmaxb,vmax_dtmaxb);
  TH2F* h_dtime_vs_Qbytes = new TH2F("dTime_vs_Qbytes","dtime vs Qbytes",
		     Nbin_tbytes,0,vmax_tbytes,Nbin_dt,0,vmax_dt);
  TH2F* h_dtime_vs_Cbytes = new TH2F("dTime_vs_Cbytes","dtime vs Cbytes",
		     Nbin_tbytes,0,vmax_tbytes,Nbin_dt,0,vmax_dt);
  TH2F* h_dtime_vs_Qbytem = new TH2F("dTime_vs_Qbytem","dtime vs Qbytes",
		     Nbin_tbytes,0,vmax_tbytes,Nbin_dtmaxb,0,vmax_dtmaxb);
  TH2F* h_dtime_vs_Cbytem = new TH2F("dTime_vs_Cbytem","dtime vs Cbytes",
		     Nbin_tbytes,0,vmax_tbytes,Nbin_dtmaxb,0,vmax_dtmaxb);
  HistBinRounding(max_endtime,Nbins,vmax);
  TH2F* h_NodeQbytes_vs_time = new TH2F("NodeQbytes_vs_time","NonCache bytes",
		     Nbins,0,vmax, 3*Nhnode,0,float(Nhnode));
  TH2F* h_NodeCbytes_vs_time = new TH2F("NodeCbytes_vs_time","Cache bytes",
		     Nbins,0,vmax, 3*Nhnode,0,float(Nhnode));
  TH2F* h_NodeDbytes_vs_time = new TH2F("NodeDbytes_vs_time","Default bytes",
		     Nbins,0,vmax, 3*Nhnode,0,float(Nhnode));
  TH2F* h_NodeMsg_vs_time = new TH2F("NodeMsg_vs_time","Msg vs time",
		     Nbins,0,vmax, Nhnode,0,float(Nhnode));
  TH2F* h_NodeReply_vs_time = new TH2F("NodeReply_vs_time","Replies vs time",
		     Nbins,0,vmax, Nhnode,0,float(Nhnode));
  TH2F* h_NodeCtor_vs_time = new TH2F("NodeCtor_vs_time","Ctor vs time",
		     Nbins,0,vmax, Nhnode,0,float(Nhnode));
  TH2F* h_NodeDflt_vs_time = new TH2F("NodeDflt_vs_time","Default vs time",
		     Nbins,0,vmax, Nhnode,0,float(Nhnode));
  TH2F* h_NodeCache_vs_time = new TH2F("NodeCache_vs_time","Cached vs time",
		     Nbins,0,vmax, Nhnode,0,float(Nhnode));
  TH2F* h_NodeNonC_vs_time = new TH2F("NodeNonC_vs_time","NonCached vs time",
		     Nbins,0,vmax, Nhnode,0,float(Nhnode));

  TH1F* h_Ctor_th = new TH1F("Ctor_th","Ctor time",2*Nbins,0,vmax);
  TH1F* h_Dtor_th = new TH1F("Dtor_th","Dtor time",2*Nbins,0,vmax);
  TH1F* h_Default_th = new TH1F("Default_th","Default time",
		                                   2*Nbins,0,vmax);
  TH1F* h_Cache_th   = new TH1F("Cache_th","Cache time",
		                                   2*Nbins,0,vmax);
  TH1F* h_NonCache_th= new TH1F("NonCache_th","NonCache vs time",
		                                   2*Nbins,0,vmax);
  // 
  // Book delta-time histograms for different command types separately 
  //
  TH1F* h_comdt[transaction::nComType]; 
  double max_gapdt = 0;
  for(int i=0;i<transaction::nComType;i++) {
    double max_dthist = max(max_comdt[i],1.); // no point to look <<1msec  
    HistBinRounding(max_dthist,Nbin_dt,vmax_dt);
    if(max_dthist>max_gapdt) max_gapdt = max_dthist;  
    char hname[10] = " ";
    sprintf(hname,"dt_comm%02d",i);
    string htitle;
    transFirst->ComName(i,htitle);
    h_comdt[i] = new TH1F(hname,htitle.c_str(),Nbin_dt,0,vmax_dt);
  }
  HistBinRounding(max_gapdt,Nbin_dt,vmax_dt);
  TH1F* h_gapdt = new TH1F("dt_gap","Command gap",Nbin_dt,0,vmax_dt);
  double max_logdt = log10(max_gapdt);
  HistBinRounding(max_logdt+2.0,Nbin_dt,vmax_dt);
  TH1F* h_gapdt_log = new TH1F("dt_loggap","Command gap",
                     Nbin_dt,-2.,vmax_dt-2);  
  //
  // Proxy busy histogram
  // 
  HistBinRounding(max_endtime,Nbins,vmax);
  int Nbin_busy = 2*Nbins;
  double tbin_busy = vmax/Nbin_busy; 
  TH1F* h_proxy_busy = new TH1F("Proxy_Busy","Proxy Busy",
		    Nbin_busy,0,vmax);
  TH1F* h_proxy_idle  = new TH1F("Proxy_Idle","Proxy Idle",
		    Nbin_busy,0,vmax);
  TH1F* h_busy_ctor     = new TH1F("Busy_Ctor","Proxy Busy Ctor",
		    Nbin_busy,0,vmax);
  TH1F* h_busy_default  = new TH1F("Busy_Default","Proxy Busy Default",
		    Nbin_busy,0,vmax);
  TH1F* h_busy_cache    = new TH1F("Busy_Cache","Proxy Busy Cache",
		    Nbin_busy,0,vmax);
  TH1F* h_busy_noncache = new TH1F("Busy_NonCache","Proxy Busy NonCache",
		    Nbin_busy,0,vmax);

  // Loop through individual transactions again to histogram
  // delta time info for each command type separately
 
  for(it=transList->begin(); it!=transList->end(); ++it) {
    transaction* aTrans = &(*it);
    double delta =1.e-3*aTrans->Delta();  // msec
    double tbytes=1.e-3*aTrans->Nbytes(); // kbytes
    double dtmaxb = min(delta,0.999*vmax_dtmaxb);  
    double ttime =aTrans->Time() - T0;
    int NodeLoc  =aTrans->Node() - NodeFirst;
    double hnode;
    if(NodeLoc<MAXNMAP) {
      hnode = nodeMap[NodeLoc];
    } else { 
      hnode = -1;
    }
    h_NodeMsg_vs_time->Fill(ttime,hnode,1.);
    if(aTrans->ComIsReply()) {
      h_NodeReply_vs_time->Fill(ttime,hnode,1.);
    } else { 
      h_gapdt->Fill(delta,1.);
      double dtlog = log10(delta);
      h_gapdt_log->Fill(dtlog,1.);      
    }
    
    // For byte counts vs time plots. Use staggered 1/3 sized bins
    // for the different types of transactions to allow clear overlay.
 
    if(aTrans->ComIsDefaultReply()) {
      h_Default_th->Fill(ttime,1.);
      h_NodeDflt_vs_time->Fill(ttime,hnode,1.);
      h_NodeDbytes_vs_time->Fill(ttime,hnode+0.2,tbytes);  
    } else if(aTrans->ComIsCtorDone()) {
      h_Ctor_th->Fill(ttime,1.);
      h_NodeCtor_vs_time->Fill(ttime,hnode,1.);
    } else if(aTrans->ComIsCacheReply()) {
      h_Cache_th->Fill(ttime,1.);
      h_NodeCache_vs_time->Fill(ttime,hnode,1.);
      h_NodeCbytes_vs_time->Fill(ttime,hnode+0.5,tbytes);  
      h_dtime_vs_Cbytes->Fill(tbytes,delta);  
      h_dtime_vs_Cbytem->Fill(tbytes,dtmaxb);  
    } else if(aTrans->ComIsNonCacheReply()) {
      h_NonCache_th->Fill(ttime,1.);
      h_NodeNonC_vs_time->Fill(ttime,hnode,1.); 
      h_NodeQbytes_vs_time->Fill(ttime,hnode+0.8,tbytes);  
      h_dtime_vs_Qbytes->Fill(tbytes,delta); 
      h_dtime_vs_Qbytem->Fill(tbytes,dtmaxb); 
    } else if(aTrans->ComIsDtor()) {
      h_Dtor_th->Fill(ttime,1.);
    }
    int ComType=aTrans->ComType();
    h_comdt[ComType]->Fill(delta,1.);

    // proxy busy time accumulation

    double stime = ttime - 1.e-3*delta;  
    int indbin1 = int(stime/tbin_busy); 
    int indbin2 = int(ttime/tbin_busy);
    for(int i=indbin1; i<indbin2+1; i++) { 
      double binmin = i*tbin_busy;
      double binmax = (i+1)*tbin_busy;
      double wtbusy = 100*(min(binmax,ttime)-max(binmin,stime))/tbin_busy;
      double bintime = 0.5*(binmin+binmax);
      if(aTrans->ComIsReply()) { 
        h_proxy_busy->Fill(bintime,wtbusy);
      } else { 
        h_proxy_idle->Fill(bintime,wtbusy);
      } 
      if(aTrans->ComIsDefaultReply()) {
        h_busy_default->Fill(bintime,wtbusy); 
      } else if (aTrans->ComIsCacheReply()) { 
        h_busy_cache->Fill(bintime,wtbusy); 
      } else if (aTrans->ComIsNonCacheReply()) { 
        h_busy_noncache->Fill(bintime,wtbusy); 
      } else if (aTrans->ComIsCtorDone()) { 
        h_busy_ctor->Fill(bintime,wtbusy); 
      }
    }   
  }

  cout << "All histograms done." << endl;

  return 0;
}
