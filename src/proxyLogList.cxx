//
// Routine to make a STL list of transactions from DBproxy log file entry 
//
//   Update:  Su Dong  2007-09-05  Initial release 
//            Su Dong  2007-09-24  Converted to class     
//            Su Dong  2007-11-04  Parse node num at start of line only
//                                 and fixed err count sign 
//            Su Dong  2008-07-14  Reduce IP mask to top 16 bits
//                                 Added more error checks & raised err limit
//
#include <list>
#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

#include "DaqDbProxyUtils/transaction.h"
class transaction;

#include "DaqDbProxyUtils/proxyLogList.h"

#if RootApp
  ClassImp(proxyLogList);
#endif

// ctor
//             
//  maskIP is the top 16 bit mask for node IP addresses e.g. 0xa920000    
//  printLevel = 0   no print
//             = 1   every 10000 lines 
//             = 2   every 1000 lines 
//             = 3   every 100 lines
//             = 4   every line 
//  
proxyLogList::proxyLogList(
char* fileName, int version, int maskIP, int printLevel)
{
  _version    = version; 
  _printLevel = printLevel;
  _maskIP     = maskIP;
  int nchar = strlen(fileName);
  memcpy(_fileName,fileName,nchar+1);
}
 
//
// Convert IP address mask to the transaction keyword prefix string 
//
void proxyLogList::PrefixString(string& sPrefix) 
{  
  char prefix[10] = " ";
  int  ChMask = (_maskIP >> 16) & 0xffff;  // only the upper 16 bits  
  sprintf(prefix,"0x%x",ChMask);  
  sPrefix.assign(prefix); 
}
//
// Dump
//
void proxyLogList::dump(int level)
{
  cout << "proxyLogList dump:" << endl;
  cout << "version       = " << _version << endl;
  cout << "print level   = " << _printLevel << endl;
  string fName;
  FileName(fName);
  cout << "log file name = " << fName << endl;
  cout << "IP addr mask  = " << hex << _maskIP << dec << endl; 

  //*** higher level list of details to be implemented  
  if(level>0) cout << "Detailed dump still to be implemented" << endl;
}

//
// Execution of log file reading and list creation 
//
int proxyLogList::execute() { 
  //
  // Initialize 
  //
  string line(120,' ');
  int numline  = 0; 
  int nummsg   = 0;
  int numacc   = 0;
  int nerror   = 0;
  bool dumpflag = 0;
  string sprefix;
  PrefixString(sprefix);
  int len_prefix = sprefix.length(); 
 
  transaction *aTrans;  
  transaction *transFirst = 0;
  transaction *transLast = 0;

  //
  // Open DBproxy log file
  //
  ifstream myfile(_fileName);

  if(!myfile.is_open()) {
    cout << "Error in opening file " << _fileName << endl; 
    return 9999;
  }
    
  // Loop over the log entries to read line by line

    while (! myfile.eof() )
    {
      getline (myfile,line);
      numline++;      
      //
      // Abort if too many errors
      //
      nerror = nummsg-numacc;    
      if( nerror>100 ) { 
        cout << "***ABORT: TOO MANY LOG FILE READ IN ERRORS***" << endl;
        return 9999;
      }
      //
      // Only analyze first line of each request/reply with node:connection tag
      //
      string::size_type NotFound = string::npos;
      string::size_type ind_node = line.find(sprefix);
      if(ind_node!=NotFound) {
      // only take this as a transaction if node number at start of line 
      // to not be confused with other messages containing the node number.  
        if(ind_node==0) {  
          nummsg++;
        } else {
          continue;
        } 
      } else { 
        continue;
      }
      // 
      // print flag check
      //
      dumpflag = 0;
      if(_printLevel>0 && nummsg%10000==1) { dumpflag = 1; } 
      else if(_printLevel>1 && nummsg%1000==1) { dumpflag = 1; }   
      else if(_printLevel>2 && nummsg%100==1) { dumpflag = 1; }   
      else if(_printLevel>3) { dumpflag = 1; }   
      if(dumpflag) {  
        cout << "Processing line " << numline << " connection " 
             << nummsg << endl;
        cout << line << endl;  
      }

      //
      // Parsing the details of the req/reply 
      //

      string snode;
      string sconnx;
      string stsec;
      string susec;
      string sdelta;
      string comment; 
      string nbytes;

      //
      // Node and connection tags
      snode    = line.substr(ind_node+len_prefix,4); 
      sconnx   = line.substr(ind_node+len_prefix+5,5); 
      // 
      // Parse the connection log text to get key word positions
      //
      string::size_type ind_time  = line.find(" time ");
      string::size_type ind_delta = line.find(" delta ");
      string::size_type ind_dot  = NotFound;
      if(ind_time!=NotFound) {  
        ind_dot  = line.find(".",ind_time);
        if(ind_dot==NotFound || ind_dot>ind_delta ) { 
          cout << "*** Time stamp .separator missing in phase: " 
               << endl << line << endl;
          nerror++; 
          continue;
        } 
      } else { 
        cout << "*** No time stamp in phrase: " << line << endl;
        nerror++;  
        continue;
      }         

      string::size_type ind_in    = NotFound;
      if(ind_delta!=NotFound) {  
        ind_in  = line.find(" in ",ind_delta);
        if(ind_in==NotFound) {
          cout << "*** The comment -in- marker missing in phrase: " 
	       << endl << line << endl;
          nerror++;  
          continue;
        }
      } else {  
        cout << "*** The delta time marker missing in phrase: " 
             << endl << line << endl;
        nerror++; 
        continue;         
      }
      string::size_type ind_bytes = line.find(" bytes ");

      // More log file syntax checks to identify corrupted entties
    
      string::size_type ind_colon = line.find(":");
      if(ind_colon>10 || ind_colon<8 ) { 
        cout << "*** Node:connex syntax error in phrase: " 
             << endl << line << endl;
        nerror++;
        continue;  
      } 
      if(ind_time-ind_colon!=6) { 
        cout << "*** Node:connex syntax error in phrase: " 
             << endl << line << endl;
        nerror++;
        continue;  
      } 
      int ndigi_time = ind_delta-ind_time;  
      if(ndigi_time<11 || ndigi_time>27 ) { 
        cout << "*** Time stamp syntax error in phrase: " 
             << endl << line << endl;
        nerror++;
        continue;  
      }
     
      // Construct the variable values strings 
     
      int nchar;
      // Time part 1 (sec)
      nchar = ind_dot - ind_time - 6;     
      stsec = line.substr(ind_time+6,nchar);
      // Time part 2 (nsec) 
      nchar = ind_delta - ind_dot - 1 - 3; // drop last 3 digit -> microsec
      susec = line.substr(ind_dot+1,nchar);
      // Delta time (nsec) 
      nchar = ind_in - ind_delta - 7 - 3;  // drop last 3 digit -> microsec
      sdelta = line.substr(ind_delta+7,nchar);
      if(line.substr(ind_in-3,3)!="000")  { 
        cout << "*** Delta time value error in phrase: " 
             << endl << line << endl;
        nerror++;
        continue;  
      }
      // comment field and byte count
      if(ind_bytes!=NotFound) {
        nbytes  = line.substr(ind_bytes+7);
        nchar   = ind_bytes - ind_in - 4;
      } else {
        nbytes  = "0";
        nchar   = line.length() - ind_in - 4; 
      }         
      if(nchar<24) {  
        comment = line.substr(ind_in+4,nchar);
      } else {
        cout << "*** Command type error in phrase: " 
             << endl << line << endl;
        nerror++;
        int nchcom = min(nchar,40); // transaction _commment limit
        comment = line.substr(ind_in+4,nchcom);
      }       
      if(nbytes.length()>8) {
        cout << "*** nBytes syntax error in phrase: " 
             << endl << line << endl;
        nerror++;
        continue;  
      }
      int ival_nbytes;
      sscanf(nbytes.c_str(),"%d",&ival_nbytes);      
      if(ival_nbytes<0 || ival_nbytes>99000000) {
        cout << "*** nBytes syntax error in phrase: " 
             << endl << line << endl;
        nerror++;
        continue;  
      }   
      //
      // Create a transaction object  
      //
      aTrans = new transaction( snode, sconnx, stsec, susec, 
                                sdelta, nbytes, comment );

      // Recalculate the Delta time to fix original DbProxy log bug
      if(transFirst!=0) {
        long long int dtusec = aTrans->diffusec(transLast);   
        long long int dtraw  = aTrans->Delta();
        if(dtusec != dtraw) {
          if(_version !=0 ) {  
	    std::cout << "Fixing Delta time raw/fix = " 
                      << dtraw <<" / "<< dtusec << std::endl;
            transLast->dump();
            aTrans->dump();
            aTrans->SetDelta(dtusec);
          }
        }
      }
      //
      // Dump error records and add transaction to the output list 
      //
      if(dumpflag) aTrans->dump();   
      _theList.push_back(*aTrans);
      numacc++;

      if(transFirst==0) { transFirst = aTrans; } 
      transLast = aTrans;
    }
 
    myfile.close();

    int merror = nummsg - numacc; 
    cout << "Number of lines          = " << numline << endl;
    cout << "Number of messages items = " << nummsg  << endl;
    cout << "Number of read in errors = " << nerror  << endl;
    cout << "Number of errored msgs   = " << merror  << endl;
    cout << "Node name prefix         = " << sprefix << endl;  
    if(nummsg>0) { 
      cout << "Time span (sec)          = " << 
        transLast->difftime(transFirst) << endl;
    } else { 
      cout << "** No transaction message. Please check domain IP mask." <<endl;
      nerror++;  
    }
    return max(merror,nerror);
}
