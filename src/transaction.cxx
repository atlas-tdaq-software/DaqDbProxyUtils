//
// DBproxy log transaction decription
//
// Update: Su Dong  2007-09-5  Initial release
//
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cstring>
using namespace std;

#include "DaqDbProxyUtils/transaction.h"

#if RootApp 
  ClassImp(transaction); 
#endif 

// default ctor 
transaction::transaction() { 
  _node = 0;
  _connection = 0;
  _time_sec = 0; 
  _time_usec = 0;
  _delta = 0;
  _nbytes = 0;
  memset(_comment,'-',6);
}  

// ctor from parsed strings  
transaction::transaction( string cnode, string connx, 
                          string tsec,  string tusec,
			  string dtime, string bytes, string comm ){
  sscanf(cnode.c_str(),"%x",&_node);
  sscanf(connx.c_str(),"%d",&_connection);
  sscanf(tsec.c_str(),"%d",&_time_sec);
  sscanf(tusec.c_str(),"%d",&_time_usec);
  sscanf(dtime.c_str(),"%lld",&_delta);  // 64 bit mode for old buggy version 
  sscanf(bytes.c_str(),"%d",&_nbytes);
  memset(_comment,'\0',40);
  int length = comm.length();
  comm.copy(_comment,length);

  // try to catch various error conditions
  if(_nbytes<0||_nbytes>9000000) {
    std::cout << "**Warning transaction Nbytes =" << _nbytes << 
      " is out of range" << std::endl;
    dump();
  }
}

// ctor for numerical initialization
transaction::transaction( int inode, int iconnx, int isec, int iusec,
			  int idtime, int ibytes, string comm){
  _node = inode;
  _connection = iconnx;
  _time_sec = isec;
  _time_usec = iusec;
  _delta = idtime;
  _nbytes = ibytes;
  int length = comm.length();
  comm.copy(_comment,length);
}

//
// Parse command type
//
int transaction::ComType() const {
  string::size_type ind_key1, ind_key2, ind_key3;
  string::size_type NotFound=string::npos;
  string comm;
  comm.assign(_comment);
  ind_key1 = comm.find("default-replied");
  ind_key2 = comm.find("default");
  if(ind_key1!=NotFound) {
    return COM_DEF_REPLY;
  } else if(ind_key2!=NotFound) {
    return COM_DEFAULT;
  }

  ind_key1 = comm.find("query-non-cache");
  ind_key2 = comm.find("query-cache");
  ind_key3 = comm.find("query");
  if(ind_key1!=NotFound) {
    return COM_Q_NONCACHE;
  } else if(ind_key2!=NotFound) {
    return COM_Q_CACHE;
  } else if(ind_key3!=NotFound) {
    return COM_QUERY;
  }

  ind_key1 = comm.find("fieldlist-non-cache");
  ind_key2 = comm.find("fieldlist-cache");
  ind_key3 = comm.find("fieldlist");
  if(ind_key1!=NotFound) {
    return COM_FL_NONCACHE;
  } else if(ind_key2!=NotFound) {
    return COM_FL_CACHE;
  } else if(ind_key3!=NotFound) {
    return COM_FIELDLIST;
  }

  ind_key1 = comm.find("ctor-done");
  ind_key2 = comm.find("ctor");
  if(ind_key1!=NotFound) {
    return COM_CTOR_DONE;
  } else if(ind_key2!=NotFound) {
    return COM_CTOR;
  }

  ind_key1 = comm.find("dtor");
  if(ind_key1!=NotFound) {
    return COM_DTOR;
  }

  return COM_UNKNOWN;
}

// Commmand name 

void transaction::ComName(const int indcom, string& Cname) { 
  if(indcom==COM_DEFAULT)               { Cname = "default"; }  
  else if(indcom==COM_DEF_REPLY)        { Cname = "default-replied"; }  
  else if(indcom==COM_CTOR)             { Cname = "ctor"; }  
  else if(indcom==COM_CTOR_DONE)        { Cname = "ctor-done"; }  
  else if(indcom==COM_DTOR)             { Cname = "dtor"; }  
  else if(indcom==COM_QUERY)            { Cname = "query"; }  
  else if(indcom==COM_Q_CACHE)          { Cname = "query-cache"; }  
  else if(indcom==COM_Q_NONCACHE)       { Cname = "query-non-cache"; }  
  else if(indcom==COM_FIELDLIST)        { Cname = "fieldlist"; }  
  else if(indcom==COM_FL_CACHE)         { Cname = "fieldlist-cache"; }  
  else if(indcom==COM_FL_NONCACHE)      { Cname = "fieldlist-non-cache"; }  
  else { Cname = "unknown"; }  
}

void transaction::ComName(string& Cname) { 
  int indcom = ComType();
  ComName(indcom,Cname);
}

// Command classifications to allow simple identification of query groups   
   
bool transaction::ComIsReply(const int indcom) const { 
  if( indcom==COM_DEF_REPLY || indcom==COM_CTOR_DONE  ||
      indcom==COM_Q_CACHE   || indcom==COM_Q_NONCACHE ||
      indcom==COM_FL_CACHE  || indcom==COM_FL_NONCACHE ) { 
    return 1;
  } else { 
    return 0;
  }     
}

bool transaction::ComIsReply() const { 
  int commType = ComType();
  return ComIsReply(commType);
} 

bool transaction::ComIsCacheReply(const int indcom) const { 
  if( indcom==COM_Q_CACHE   || indcom==COM_FL_CACHE ) { 
    return 1;
  } else { 
    return 0;
  }     
}

bool transaction::ComIsCacheReply() const { 
  int indcom = ComType();
  return ComIsCacheReply(indcom);
}

bool transaction::ComIsNonCacheReply(const int indcom) const { 
  if( indcom==COM_Q_NONCACHE || indcom==COM_FL_NONCACHE ) { 
    return 1;
  } else { 
    return 0;
  }     
}

bool transaction::ComIsNonCacheReply() const { 
  int indcom = ComType();
  return ComIsNonCacheReply(indcom);
}

bool transaction::ComIsDefaultReply(const int indcom) const { 
  if( indcom==COM_DEF_REPLY ) { 
    return 1;
  } else { 
    return 0;
  }     
}

bool transaction::ComIsDefaultReply() const { 
  int indcom = ComType();
  return ComIsDefaultReply(indcom);
}

bool transaction::ComIsCtorDone(const int indcom) const { 
  if( indcom==COM_CTOR_DONE ) { 
    return 1;
  } else { 
    return 0;
  }     
}

bool transaction::ComIsCtorDone() const { 
  int indcom = ComType();
  return ComIsCtorDone(indcom);
}

bool transaction::ComIsDtor(const int indcom) const { 
  if( indcom==COM_DTOR) { 
    return 1;
  } else { 
    return 0;
  }     
}

bool transaction::ComIsDtor() const { 
  int indcom = ComType();
  return ComIsDtor(indcom);
}

// comparison < operator 

int transaction::operator<(const transaction& aTrans) const { 
  if(Node()<aTrans.Node()) {
    return 1;
  } else if(Node()>aTrans.Node()) {
    return 0;
  } else {
    if(Connection()<aTrans.Connection()) {
      return 1;
    } else {
      return 0; 
    }
  }
  return 0;
} 

// print dump

void transaction::dump() { 
  printf(" Node:Connx=%3x:%5d  Time=%10i.%6i delta=%6lld bytes=%5i comm=",
	 _node,_connection,_time_sec,_time_usec,Delta(),_nbytes);
  string commstr;
  Comment(commstr);
  std::cout << commstr << std::endl;    
  return;
}

// time difference in (Second) between two transactions

double transaction::difftime( const transaction* startTrans ) { 
  double diffsec  = TimeSec()  - startTrans->TimeSec(); 
  double diffusec = TimeUsec() - startTrans->TimeUsec();
  double diffall = 1.e-6*diffusec + diffsec;     
  return diffall;  
}

// time difference in (microSec) between two transactions

long long int transaction::diffusec( const transaction* startTrans ) { 
  long long int diffsec  = TimeSec()  - startTrans->TimeSec(); 
  long long int diffusec = TimeUsec() - startTrans->TimeUsec();
  return 1000000*diffsec+diffusec; 
}
