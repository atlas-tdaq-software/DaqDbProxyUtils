//
// DbProxy log Histogram 
//
// Update: Su Dong  2007-09-24 Initial class version  
//
#ifndef PROXYHIST_HH
#define PROXYHIST_HH

class proxyHist {

public: 

  proxyHist();
  virtual ~proxyHist(){}; 

  double TimeMax() const { return _timemax; }
  void   SetTimeMax(double tmax) { _timemax = tmax; }   

  void HistBinRounding(double vax, int& Nbin, double& rmax);
  void OutputName(char* fileName, string& outName);   
  int execute( char* fileName, int version, int domain );

private:

  double _timemax;  // maximum time history window (sec)
                    // -ve value means dynamic max from data 

#if RootApp
   ClassDef(proxyHist,1)
#endif

};
#endif 


  
