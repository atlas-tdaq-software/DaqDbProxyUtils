//
// DbProxy log transaction list
//
// Update: Su Dong  2007-09-24 Initial class version  
//
#ifndef PROXYLOGLIST_HH
#define PROXYLOGLIST_HH

#include <string>
#include <list> 
#include "DaqDbProxyUtils/transaction.h" 

class proxyLogList {

public: 

  proxyLogList(char* fileName, int version, int maskIP, int printLevel);
  virtual ~proxyLogList(){}; 
  
  int   Version() const { return _version; } 
  int   PrintLevel() const { return _printLevel; } 
  int   MaskIP() const { return _maskIP; } 
  void  PrefixString(string& sPrefix);  
  void  FileName(string& fName) { fName.assign(_fileName); } 
  list<transaction> *TheList() { return &_theList; } 

  void SetVersion(int version) { _version = version; } 
  void SetPrintLevel(int printLevel) { _printLevel = printLevel; } 
  void SetMaskIP(int maskIP) { _maskIP = maskIP; } 

  int  execute();
  void dump(int level);

 private:

  int   _version;        // log file format version
                         //   0 = original may/2007 with time stamp bug
                         //   1 = Aug/2007 version: time stamp bug fixed   
  int   _printLevel;     // print level flag
                         //   0 = no print
                         //   1 = every 10000 transaction entries.
                         //   2 = every  1000 transaction entries. 
  int   _maskIP;         // upper 20 bits of the IP address word 
                         // e.g. 0xa922000
  char  _fileName[200];  // full path filename of the DBproxy log file 
  list<transaction> _theList;  // the log transaction list 

#if RootApp
   ClassDef(proxyLogList,1)
#endif

};
#endif 

  
