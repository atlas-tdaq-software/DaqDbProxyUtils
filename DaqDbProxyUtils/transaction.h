//
// DbProxy log transaction decription
//
// Update: Su Dong  2007-09-05 Initial release  
//
#ifndef TRANSACTION_HH
#define TRANSACTION_HH

#include <string> 

class transaction {

public: 

  transaction();
  transaction( string cnode, string connx,  string tsec, string tusec,
               string dtime, string bytes, string comm );
  transaction( int inode, int iconnx, int isec, int iusec,
               int idtime, int ibytes, string comm);

  virtual ~transaction(){}; 
   
  //  Accessors:
  //  Node()            Node number after stripping th prefix 
  //  Connection()      Connection ID number
  //  TimeSec()          The absolute time in sec at initeger sec level 
  //  TimeUsec()        The absolute time residual in microsec  
  //  Time()            full absolute analog time in sec
  //  Delta()           transaction delta time in microsec
  //  Nbytes()          query bytes 
  //  SetDelta(dtusec)  Set the Delta time from a recalculation   
  //  Comments(CommOut) transaction command string 
  //  ComType()         parsed transaction command type   
  //  ComName(indcom,Cname)  Command name text string for a ommand index
  //  ComName(Cname)         Get the command name text string
  //  ComIsReply()           command is execution reply (Delta=execution time)
  //  ComIsCacheReply()      command is execution reply of cached query
  //  ComIsNonCacheReply()   command is execution reply of non-cached query
  //  ComIsDefaultReply()    command is execution reply of default request
  //  ComIsCtorDone()        command is execution done of connection Ctor
  //  difftime(startTrans)   time difference (sec) since another transaction
  //  diffusec(startTrans)   time difference (usec) since another transaction
  //  dump()            print the transaction content 
  //            
  int  Node() const { return _node; } 
  int  Connection() const { return _connection; } 
  int  TimeSec() const { return _time_sec; } 
  int  TimeUsec() const { return _time_usec; }  
  double  Time() const { return _time_sec + 1e-6*_time_usec; } 
  long long int  Delta() const { return _delta; }
  void SetDelta(long long int dtusec) { _delta = dtusec; }   
  int  Nbytes() const { return _nbytes; } 
  void Comment(string& CommOut) { CommOut.assign(_comment); }     
  int  ComType() const;  
  void ComName(const int indcom, string& Cname);  
  void ComName(string& Cname);  
  bool ComIsReply() const;
  bool ComIsReply(const int indcom) const;
  bool ComIsCacheReply() const;
  bool ComIsCacheReply(const int indcom) const;
  bool ComIsNonCacheReply() const;
  bool ComIsNonCacheReply(const int indcom) const;
  bool ComIsDefaultReply() const;
  bool ComIsDefaultReply(const int indcom) const;
  bool ComIsCtorDone() const;
  bool ComIsCtorDone(const int indcom) const;
  bool ComIsDtor() const;
  bool ComIsDtor(const int indcom) const;

  int operator<(const transaction& aTrans) const;

  double difftime( const transaction* startTrans );
  long long int diffusec( const transaction* startTrans );
  void dump();

  enum { nComType = 12,
         COM_DEFAULT=0,
         COM_DEF_REPLY=1,
         COM_QUERY=2,
         COM_Q_CACHE=3,
         COM_Q_NONCACHE=4,
         COM_FIELDLIST=5,
         COM_FL_CACHE=6,
         COM_FL_NONCACHE=7,
         COM_CTOR=8,
         COM_CTOR_DONE=9,
         COM_DTOR=10,
         COM_UNKNOWN=11 };

 private:

   unsigned _node;
   int   _connection;
   int   _time_sec;
   int   _time_usec;
   long long int  _delta;  // 64 bit mode to deal with old buggy values
   int   _nbytes; 
   char  _comment[41];

#if RootApp
   ClassDef(transaction,1)
#endif

};
#endif 

  
