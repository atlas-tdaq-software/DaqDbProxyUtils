//
// DBproxy log summary class of a complete connection
//
// Update: Su Dong  2007-09-05 Initial release
//
#ifndef CONNECTIONSUMMARY_HH
#define CONNECTIONSUMMARY_HH

#include <string> 
#include "transaction.h"

class connectionSummary {

public: 

  connectionSummary();
   
  virtual ~connectionSummary(){}; 
   
  int  Node() const { return _node; } 
  int  Connection() const { return _connection; } 
  double  TimeStart() const { return _time_start; } 
  double  TimeEnd() const { return _time_end; } 
  int  Nmessage() const { return _nmessage; } 
  int  ComBytes(const int ComType) const { return _com_sumbytes[ComType]; }  
  int  AllBytes() const;
  int  NonCacheBytes() const;
  int  CacheBytes() const;
  double ComTime(const int ComType) const { return _com_sumtime[ComType]; } 
  double AllTime() const;
  double NonCacheTime() const;
  double CacheTime() const;  
  
  transaction* TransInit() const { return _transInit; } 
  int Initialize(transaction* trans);
  int Update(transaction* trans);

  void dump();
  void summary(const double T0);
  
 private:

   int    _node;
   int    _connection;
   double _time_start;
   double _time_end;
   int    _nmessage;
   int    _com_mult[transaction::nComType];
   int    _com_sumbytes[transaction::nComType];
   double _com_sumtime[transaction::nComType];
 
   transaction* _transInit;

#if RootApp
   ClassDef(connectionSummary,1)
#endif

};
#endif 

  
