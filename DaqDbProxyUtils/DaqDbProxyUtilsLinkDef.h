#ifdef __CINT__ 
#pragma link off all globals;    
#pragma link off all classes;    
#pragma link off all functions;   

#define RootApp 1 
#pragma link C++ class transaction+;     
#pragma link C++ class connectionSummary+;  
#pragma link C++ class proxyLogList+;  
#pragma link C++ class proxyConnxList+; 

#pragma link C++ class proxyHist+;  

#endif
