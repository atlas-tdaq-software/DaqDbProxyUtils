//
// DbProxy log transaction list
//
// Update: Su Dong  2007-09-24 Initial class version  
//
#ifndef PROXYCONNXLIST_HH
#define PROXYCONNXLIST_HH

#include <string>
#include <list> 
#include "DaqDbProxyUtils/transaction.h" 
#include "DaqDbProxyUtils/connectionSummary.h" 

class proxyConnxList {

public: 

  proxyConnxList(int printLevel, list<transaction>* transList);
  virtual ~proxyConnxList(){}; 
  
  int   PrintLevel() const { return _printLevel; } 
  list<transaction> *TransList() { return _transList; } 
  list<connectionSummary> *ConnxList() { return &_connxList; } 

  void SetPrintLevel(int printLevel) { _printLevel = printLevel; } 

  int execute();
 
 private:

  int   _printLevel;     // print level flag
                         //   0 = no print
                         //   1 = two line per connection summary
                         //   2 = detailed table summary dump per connection 
  list<transaction>* _transList;  // the input unsorted transaction list 
                                  // which will be sorted at output 
  list<connectionSummary> _connxList; // output connection summary list 

#if RootApp
   ClassDef(proxyConnxList,1)
#endif

};
#endif 

  
