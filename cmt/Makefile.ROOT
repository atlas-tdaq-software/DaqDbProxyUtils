# -------------------------------------------------------------
# This makefile is designed to compile a ROOT library
# independent of the Atlas framework.  You only need to
# have ROOTSYS pointing to a valid ROOT installation.
# This has been tested on Linux and OS X, but should work
# for any architecture supported in ROOT Makefile.arch
#
# Update: Su Dong  2007-09-05 Adapted from TrigTauPerformNtuple
#
# --- External configuration ----------------------------------
include $(ROOTSYS)/test/Makefile.arch

MFLAGS   = -MM -Wall -W -Woverloaded-virtual
CXXFLAGS = -m32 -fPIC

# -------------------------------------------------------------

BASEDIR  = ..
WORKDIR  = $(BASEDIR)/tmp
LIBDIR   = $(BASEDIR)/rootlib

# Internal configuration
PACKAGE=DaqDbProxyUtils
LD_LIBRARY_PATH:=$(ROOTSYS)/lib
INCDIR=$(BASEDIR)/$(PACKAGE)
OBJDIR=$(WORKDIR)
SRCDIR=$(BASEDIR)/src

# The following specifies where make looks for prerequisites
# VPATH=$(INCDIR) $(SRCDIR) $(OBJDIR)

# Better to do it this way?
vpath %.h   $(INCDIR)
vpath %.cxx $(SRCDIR)
vpath %.C   $(SRCDIR)
vpath %.o   $(OBJDIR)
vpath %.d   $(WORKDIR)

INCLUDES +=  -I$(INCDIR) -I$(ROOTSYS)/include -I$(INCDIR)/..
ROOTSYS  ?= ERROR_RootSysIsNotDefined
HHLIST    = $(filter-out $(PACKAGE)LinkDef.h, $(patsubst $(INCDIR)/%h, %h, $(wildcard $(INCDIR)/*.h)))

CINTFILE  = $(PACKAGE)Cint.cxx
CINTOBJ   = $(PACKAGE)Cint.o
SHLIBFILE = $(LIBDIR)/lib$(PACKAGE)_ROOT.$(DllSuf)

# get libraries of ROOT
define ldlinksuffixROOT
   $(addsuffix $(LDLINKSUFFIX),$(Lib)) $(shell if [ "$(findstring -Ldlink2,$(OPTIONS))" ]; then echo $(addsuffix _pkgid_$(ROOTVER),$(Lib)); fi)
endef

default: shlib
#default: testvars

# List of all source files to build
CCLIST=$(filter-out $(SKIPLIST),$(patsubst $(SRCDIR)/%cxx, %cxx, $(wildcard $(SRCDIR)/*.cxx)))
CMLIST=$(filter-out $(SKIPLIST),$(patsubst $(SRCDIR)/%C, %C, $(wildcard $(SRCDIR)/*.C)))

# List of all object files to build
OLIST=$(patsubst %.cxx,%.o,$(CCLIST)) $(patsubst %.C,%.o,$(CMLIST))

# List of all dependency file to make
DLIST=$(patsubst %.h,%.d,$(HHLIST))

testvars:
	@echo $(HHLIST)
	@echo $(CCLIST)
	@echo $(OLIST)
	@echo $(DLIST)

# Implicit rule making all dependency Makefiles included at the end of this makefile
$(WORKDIR)/%.d: %.cxx $(HHLIST)
	@echo "Making $@"
	@mkdir -p $(WORKDIR)
	@set -e; $(CC) $(MFLAGS) $(CPPFLAGS) $(INCLUDES) $< \
	  | sed 's/\($(notdir $*)\)\.o[ :]*/\1.o $(notdir $@) : /g' > $@; \
	  [ -s $@ ] || rm -f $@
$(WORKDIR)/%.d: %.C $(HHLIST)
	@echo "Making $@"
	@mkdir -p $(WORKDIR)
	@set -e; $(CC) $(MFLAGS) $(CPPFLAGS) $(INCLUDES) $< \
	  | sed 's/\($(notdir $*)\)\.o[ :]*/\1.o $(notdir $@) : /g' > $@; \
	  [ -s $@ ] || rm -f $@

# Implicit rule to compile all classes
%.o : %.cxx
	@echo "Compiling $<"
	@mkdir -p $(OBJDIR)
	$(CXX) $(CXXFLAGS) -c $< -o $(OBJDIR)/$(notdir $@) $(INCLUDES)
%.o : %.C
	@echo "Compiling $<"
	@mkdir -p $(OBJDIR)
	$(CXX) $(CXXFLAGS) -c $< -o $(OBJDIR)/$(notdir $@) $(INCLUDES)

# Rule to make ROOTCINT output file
$(OBJDIR)/$(CINTOBJ): $(HHLIST) $(INCDIR)/$(PACKAGE)LinkDef.h
	@mkdir -p $(OBJDIR)
	@echo "Running rootcint"
	@ROOTSYS=$(ROOTSYS)
	@export ROOTSYS
	@echo $(ROOTSYS)/bin/rootcint -f $(WORKDIR)/$(CINTFILE) -c $(INCLUDES) \
		$(HHLIST) $(PACKAGE)LinkDef.h
	$(ROOTSYS)/bin/rootcint -f $(WORKDIR)/$(CINTFILE) -c $(INCLUDES) \
		$(HHLIST) $(PACKAGE)LinkDef.h
	@echo "Compiling $(CINTFILE)"
	@$(CXX) $(CXXFLAGS) -c $(WORKDIR)/$(CINTFILE) -o $(OBJDIR)/$(CINTOBJ) \
		$(INCLUDES)

# Rule to combine objects into a shared library
$(SHLIBFILE): $(OLIST) $(patsubst %.cxx,%.o,$(CINTFILE))
	@echo "Making $(SHLIBFILE)"
	@rm -f $(SHLIBFILE)
ifeq ($(PLATFORM),macosx)
	$(LD) $(SOFLAGS) $(addprefix $(OBJDIR)/, $(OLIST)) $(OBJDIR)/$(CINTOBJ) -o $(SHLIBFILE)
	cd ..; ln -sf lib$(PACKAGE)_ROOT.$(DllSuf) lib$(PACKAGE)_ROOT.so
else
	$(CXX) $(CXXFLAGS) $(addprefix $(OBJDIR)/, $(OLIST)) $(OBJDIR)/$(CINTOBJ) -shared -o $(SHLIBFILE)
endif

# Useful build targets
shlib: $(SHLIBFILE)

clean:
	rm -rf $(WORKDIR)
	rm -f $(SHLIBFILE)
ifeq ($(PLATFORM),macosx)
	rm -f $(patsubst %.$(DllSuf),%.so,$(SHLIBFILE))
endif

.PHONY : shlib lib default clean

-include $(addprefix $(WORKDIR)/,$(DLIST))

